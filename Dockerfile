FROM node:lts-alpine
WORKDIR /usr/src/app
COPY  *.json ./
COPY svg.ts.d ./
RUN npm install
COPY . .
EXPOSE 3000
CMD ["npm", "start"]

