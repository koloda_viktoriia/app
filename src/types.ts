export interface Message {
    id:string ; 
    userId:string;
    avatar : string;
    user: string;
    text: string;
    createdAt: any;
    editedAt: any;
 }
 export interface Like{
   id:string;
   userId:string;
 }
 export interface editMSG{
     id: string;
     text: string; 
 }
 export interface User{
   userId:string;
   avatar:string;
   user: string;
 }
 export interface ChatState{
     messages: Message[];
     likes: Like[];
     userProfile:User;
     isEdit: Boolean;
     editMessage: editMSG;
 }