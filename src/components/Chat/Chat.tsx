import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import Header from '../Header/Header';
import ChatHeader from '../ChatHeader/ChatHeader';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import MessageEdit from '../MessageEdit/MessageEdit';
import Footer from '../Footer/Footer';
import './Chat.css';
import * as actions from '../../redux/actions';
import * as types from '../../types';

const logo = require('./logo.svg') as string;

const Chat = () => {
  const messages: types.Message[] = useSelector((state: types.ChatState) => state.messages, shallowEqual);
  const likes: types.Like[] | undefined = useSelector((state: types.ChatState) => state.likes, shallowEqual);
  const userProfile: types.User = useSelector((state: types.ChatState) => state.userProfile, shallowEqual);
  const isEdit: Boolean = useSelector((state: types.ChatState) => state.isEdit, shallowEqual);
  const dispatch = useDispatch();
  const [isLoading, setisLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => setisLoading(false), 500);
  });


  messages?.sort(
    (a, b) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
    });

  const showEdit = (text?: string, id?: string) => {
    if (id === undefined || text === undefined) {
      const userMessages = messages
        .filter(message => message.userId === userProfile.userId);
      if (userMessages.length === 0) return;
      const message = userMessages.reduce((prev, current) => (prev.createdAt > current.createdAt) ? prev : current);
      text = message.text;
      id = message.id;
    }
    dispatch(actions.showEdit(text, id));
  }

  const addMessage = (text: string) => {
    dispatch(actions.addMessage(text));
  }

  const deleteMessage = (id: string) => {
    dispatch(actions.deleteMessage(id));
  }

  const liked = (userId: string, id: string, isLike: Boolean) => {
    dispatch(actions.liked(userId, id, isLike));
  }

  return (isLoading
    ?
    <div className="Spinner">
      <img src={logo} className="Spinner-logo" alt="logo" />
    </div>
    :
    <>
      <Header />
      <div className="Chat">
        <ChatHeader data={messages} />
        <MessageList data={messages}
          deleteMessage={deleteMessage}
          likes={likes}
          showEdit={showEdit}
          userId={userProfile?.userId}
          liked={liked} />
        <MessageInput addMessage={addMessage} showEdit={showEdit} />
      </div>
      {isEdit ? <MessageEdit /> : null}
      <Footer />

    </>
  );

}

export default Chat;
