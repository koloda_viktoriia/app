import React, { useState } from 'react';
import './MessageEdit.css';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import * as actions from '../../redux/actions';
import * as types from '../../types';

const MessageEdit = () => {
  var message: types.editMSG | undefined = useSelector((state:types.ChatState) => state.editMessage, shallowEqual);
  const [textValue, setTextValue] = useState(message?.text);
  const dispatch = useDispatch();

  const onCancel = () => {
    dispatch(actions.hideEdit());
  }

  const onEdit = () => {
    if(textValue!==undefined && message?.id!==undefined){
    dispatch(actions.editMessage(textValue, message?.id));
    dispatch(actions.hideEdit());
    }
  }

  return (

    <>
      <div className="Modal-background">
        <div id="Modal">
          <div className="Modal-header">
            <span>Edit message</span>
          </div>
          <div className="Modal-edit">
            <textarea
              onChange={(ev) => setTextValue(ev.target.value)}
              value={textValue}/>
            <div className="Modal-buttons">
              <button onClick={onCancel} className="cancelButton">Cancel</button>
              <button onClick={onEdit} className="editButton">Edit</button>
            </div>
          </div>
        </div>
      </div>

    </>
  );
}



export default MessageEdit;
