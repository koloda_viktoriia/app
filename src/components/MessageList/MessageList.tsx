import React, { useRef, useEffect, FunctionComponent } from 'react';
import './MessageList.css';
import Message from '../Message/Message';
import * as types from '../../types';
interface MessageListProps {
  data: types.Message[];
  likes: types.Like[] | undefined;
  deleteMessage: (id:string)=>void;
  showEdit: (text?: string, id?:string) => void;
  liked: (userId:string, id: string, isLike:Boolean)=>void;
  userId: string;
}

const MessageList = (props: MessageListProps) => {
  const messagesEndRef = useRef<HTMLInputElement | null>(null);

  const scrollToBottom = () => {
    if (null != messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView();
    }
  }

  useEffect(scrollToBottom, [props.data]);

  const dates = props.data?.map(item => new Date(item.createdAt).toLocaleDateString());
  let currentDate = props.data ?props.data[0].createdAt : new Date().getTime();

  const checkDate = (date: any) => {
    const messageDate = new Date(date).toLocaleDateString();
    if (new Date(currentDate).toLocaleDateString() !== messageDate) {
      currentDate = date;
      return (<span className="Message-date">{convertDate(currentDate)}</span>);
    }
  }

  const convertDate = (date: any) => {
    const currentDay = new Date(date).toLocaleDateString();
    const today = new Date().toLocaleDateString();
    const yesterday = new Date(new Date().getDate() - 1).toLocaleDateString();
    if (currentDay === today) {
      return ("Today");
    } else if (currentDay === yesterday) {
      return ("Yesterday");
    } else {
      return new Date(date).toLocaleDateString();
    }
  }

  console.log(props.data);
  return (
    <div className="Message-list">
      <span className="Message-date">{convertDate(currentDate)}</span>
      {props.data?.map(message => {
        let isLike=  false;
        if(props.likes!=undefined){
        const like = props.likes
          .find(like => {
            return ((like.userId === props.userId) && (like.id === message.id))
          });
         isLike= (like !== undefined);
        }
          return (
          <>
            {checkDate(message.createdAt)}
            <Message
              isLike={isLike}
              msg={message}
              deleteMessage={props.deleteMessage}
              showEdit={props.showEdit}
              liked={props.liked}
              userId={props.userId}
            />
          </>);
      })}
      <div ref={messagesEndRef} />
    </div>
  );

}

export default MessageList;
